# MiFlora2MQTT

Target of this project is to have a simple gateway between Mi Flora devices and an MQTT-broker.  This gives the possibility to include some more smart devices in our home automation systems.

## Process
Due to the fact that the ESP32 seems to have some problems interacting with BLE devices when it is connected to WiFi this project follows the process as described now:
1. Search for Mi Flora devices
1. Loop all found Mi Flora devices
   1. Connect to the data-service
   1. Write the magic packet
   1. Read the sensor values, manipulate and store them
1. Connect WiFi
1. Connect MQTT
1. Process stored sensor values
1. Hibernate (including gracefully disconnecting from MQTT and WiFi)

## Possible improvements
- Make the search-part optional to save some energy
- Introduce a webinterface
- Provide a possibility to change known MACs without reflashing
- ~~Publish NEW and UNKNOWN devices to the system-topic~~ => done@09.04.2019
- ~~Query the battery status and provide it~~ => done@09.04.2019
- ~~Introduce a system-topic~~ => done@08.04.2019 (including online/offline-message and LWT)
- ~~Check if the MAC of the found Mi Flora device is in the list of known devices~~ => done@08.04.2019