#include "config.h"
const int knowndevices = sizeof(FLORA_MACS)/sizeof(char *);

float temperatures[knowndevices];
int moistures[knowndevices];
int16_t lights[knowndevices];
int conductivities[knowndevices];
int batteries[knowndevices];
String newFloras[BLE_NEWSLOTS];

#include "wifi.h"
#include "ble.h"
#include "mqtt.h"

void hibernate() {
  delay(HIBERNATEDELAY);
  Serial.println("Preparing sleep");
  if (mqttclient.connected()) {
    mqttclient.publish(MQTT_SYSTEMTOPIC, MQTT_LWT_MESSAGE);
    mqttclient.disconnect();
    Serial.println("  Disconnected MQTT");
  } else {
    Serial.println("  MQTT not connected...no need to disconnect");
  }
  if (WiFi.status() == WL_CONNECTED) {
    WiFi.disconnect(true);
    Serial.println("  Disconnected WiFi");
  } else {
    Serial.println("  WiFi not connected...no need to disconnect");
  }
  Serial.print("  Going to sleep...see you in ");
  Serial.print(HIBERNATEDURATION);
  Serial.println(" minutes");
  esp_sleep_enable_timer_wakeup(HIBERNATEDURATION * 60 * 1000000ll);
  esp_deep_sleep_start();
}

void setup() {
  Serial.begin(115200);
  Serial.println("Starting MiFlora2MQTT");
  BLEDevice::init("");

  searchMiFloras();
  readMiFloras();

  if (connectWifi()) {
    if (connectMQTT()) {
      transmitSensordata();
      transmitNewFloras();
    }
  }
  
  hibernate();
} // End of setup.


/**
 * loop
 * this is the standard function of Arduino
 * in our case it is empty because all the actions
 * are done once
 */
void loop() {
  delay(1000);
} // End of loop
