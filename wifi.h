#include <WiFi.h>

WiFiClient wificlient;  // this variable will be used later on in the mqtt-part

bool connectWifi() {
  int retries = 0;
  
  Serial.println("Connecting to WiFi");
  Serial.print("  SSID: ");
  Serial.println(WIFI_SSID);

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  Serial.print("  Waiting to be connected ");
  while (WiFi.status() != WL_CONNECTED && retries < WIFI_RETRIES) {
      delay(WIFI_CONNECTIONDELAY);
      retries++;
      Serial.print(".");
  }

  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("");
    Serial.println("  WiFi connected");
    Serial.print("  IP address: ");
    Serial.println(WiFi.localIP());
  } else {
    Serial.println("  Failed to connect to WiFi");
  }

  return (WiFi.status() == WL_CONNECTED);
}
