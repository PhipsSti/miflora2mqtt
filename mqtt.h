#include <PubSubClient.h>

PubSubClient mqttclient(wificlient);

bool connectMQTT() {
  Serial.println("Connecting to MQTT");
  mqttclient.setServer(MQTT_HOST, MQTT_PORT);
  int retries = 0;

  while (!mqttclient.connected() && retries < MQTT_RETRIES) {
    if (!mqttclient.connect(MQTT_CLIENTID, MQTT_USER, MQTT_PASSWORD, MQTT_SYSTEMTOPIC, MQTT_LWT_QOS, MQTT_LWT_RETAIN, MQTT_LWT_MESSAGE)) {
      Serial.print("  MQTT connection failed:");
      Serial.print(mqttclient.state());
      Serial.println("  Retrying...");
      retries++;
    } else {
      Serial.println("  MQTT connected");
      mqttclient.publish(MQTT_SYSTEMTOPIC, MQTT_ONLINEMSG);
    }
  }
  
  return mqttclient.connected();
}

void transmitSensordata () {
  Serial.println("Starting to transmit sensordata");
  for (int i = 0; i < knowndevices && myDevices[i] != nullptr; i++) {
    Serial.print("  Processing #");
    Serial.println(i);
    
    String basetopic = MQTT_BASETOPIC;
    basetopic = basetopic + myDevices[i]->getAddress().toString().c_str() + "/";
    Serial.print("    Topic: ");
    Serial.println(basetopic);
    char buffer[64];  // this variable will be used for the conversion of float, int, etc. to string because String would fragment storage
    snprintf(buffer, 64, "%f", temperatures[i]);
    mqttclient.publish((basetopic + "temperature").c_str(), buffer);
    snprintf(buffer, 64, "%d", moistures[i]); 
    mqttclient.publish((basetopic + "moisture").c_str(), buffer);
    snprintf(buffer, 64, "%d", lights[i]);
    mqttclient.publish((basetopic + "light").c_str(), buffer);
    snprintf(buffer, 64, "%d", conductivities[i]);
    mqttclient.publish((basetopic + "conductivity").c_str(), buffer);
    snprintf(buffer, 64, "%d", batteries[i]);
    mqttclient.publish((basetopic + "battery").c_str(), buffer);
  }
}

void transmitNewFloras () {
  Serial.println("Starting to transmit NEW Mi Floras");
  for (int i = 0; i < BLE_NEWSLOTS && newFloras[i] != nullptr; i++) {
    mqttclient.publish(MQTT_NEWTOPIC, newFloras[i].c_str());
  }
}
