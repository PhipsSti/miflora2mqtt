#include "BLEDevice.h"

// The remote service we want to connect to
static BLEUUID discoveryUUID(UUID_SERVICE_DISCOVERY);
static BLEUUID dataUUID(UUID_SERVICE_DATA);
// The characteristics we are looking for
static BLEUUID uuid_battery(UUID_CHARACTERISTIC_BATTERY);
static BLEUUID uuid_sensordata(UUID_CHARACTERISTIC_SENSORS);
// The characteristic we have to use to get data
static BLEUUID uuid_write_mode(UUID_CHARACTERISTIC_WRITE);

static BLEAdvertisedDevice* myDevices[knowndevices];
int floraindex = 0; // This variable is used to 

bool isKnownDevice (const char mac[]) {
  Serial.print("  Checking if we know device with MAC: ");
  Serial.println(mac);
  bool known = false;
  for (int i = 0; i < knowndevices && known == false; i++) {
    if (strcmp(mac, FLORA_MACS[i]) == 0) {
      known = true;
    }
  }
  return known;
}

class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
  /**
  * onResult
  * this function is called everytime a mi flora is found during a scan
  */
  void onResult(BLEAdvertisedDevice advertisedDevice) {
   // We have found a device, let us now see if it contains the service we are looking for.
   if (advertisedDevice.haveServiceUUID() && advertisedDevice.isAdvertisingService(discoveryUUID)) {
    // TODO: check if flora is in whitelist
    if ( isKnownDevice(advertisedDevice.getAddress().toString().c_str()) ) { 
      Serial.print("  Known Mi Flora found: ");
      //Store the known device in our array to be able to access it later on
      myDevices[floraindex] = new BLEAdvertisedDevice(advertisedDevice);
      //increase the index so that we don't overwrite an existing device
      floraindex++;
    } else {
      Serial.print("  NEW Mi Flora found: ");
      int i = 0;
      while (i < (sizeof(newFloras)/sizeof(char*)) && newFloras[i] != nullptr) {
        i++;
      }
      newFloras[i] = advertisedDevice.getAddress().toString().c_str();
    }
    Serial.println(advertisedDevice.toString().c_str());
   } // Found our server
  } // onResult
}; // MyAdvertisedDeviceCallbacks

void searchMiFloras() {
  Serial.println("Initializing scan");
  BLEScan* pBLEScan = BLEDevice::getScan();
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setInterval(1349);
  pBLEScan->setWindow(449);
  pBLEScan->setActiveScan(true);
  pBLEScan->start(BLE_SEARCHDURATION, false);
}

class MyClientCallback : public BLEClientCallbacks {
  void onConnect(BLEClient* pclient) {
   Serial.println("    Client connected");
  }

  void onDisconnect(BLEClient* pclient) {
   Serial.println("    Client disconnected");
  }
};

void readSensordata (BLERemoteService* FloraService) {
  Serial.println("    Reading sensors now");
  static BLERemoteCharacteristic* FloraCharacteristic;
  
  // Obtain a reference to the characteristic in the service of the remote BLE server.
  FloraCharacteristic = FloraService->getCharacteristic(uuid_write_mode);
  if (FloraCharacteristic != nullptr) {
    Serial.println("      Found our characteristic for the write-mode");
    // send the magic packet to the Mi Flora
    uint8_t buf[2] = {0xA0, 0x1F};
    FloraCharacteristic->writeValue(buf, 2, true);
    Serial.println("      Sent the magic packet");
  
    FloraCharacteristic = FloraService->getCharacteristic(uuid_sensordata);
    if (FloraCharacteristic != nullptr) {
      Serial.println("      Found our characteristic for the sensordata");
  
      // Slice the string into characters
      const char *val = FloraCharacteristic->readValue().c_str();
  
      Serial.print("      Received values in HEX: ");
      for (int i = 0; i < 16; i++) {
        Serial.print((int)val[i], HEX);
        Serial.print(" ");
      }
      Serial.println(" ");
  
      temperatures[floraindex] = (*(int16_t*)val) / ((float)10.0);
      Serial.print("      Temperature: ");
      Serial.println(temperatures[floraindex]);
    
      moistures[floraindex] = val[7];
      Serial.print("      Moisture: ");
      Serial.println(moistures[floraindex]);
    
      lights[floraindex] = val[3] + val[4] * 256;
      Serial.print("      Light: ");
      Serial.println(lights[floraindex]);
     
      conductivities[floraindex] = val[8] + val[9] * 256;
      Serial.print("      Conductivity: ");
      Serial.println(conductivities[floraindex]);
    } else {
      Serial.println("      Failed to find our characteristic for the sensordata");
    }
   } else {
    Serial.print("      Failed to find our characteristic for the write-mode");
   }
  delay(500);
}

void readBattery (BLERemoteService* FloraService) {
  Serial.println("    Reading battery now");
  BLERemoteCharacteristic* batterycharacteristic = FloraService->getCharacteristic(uuid_battery);
  if (batterycharacteristic != nullptr) {
    Serial.println("      Found our characteristic for the batterydata");
    const char *battery = batterycharacteristic->readValue().c_str();

    batteries[floraindex] = battery[0];
    Serial.print("      Battery: ");
    Serial.println(batteries[floraindex]);
  } else {
    Serial.println("      Failed to find our characteristic for the batterydata");
  }
}

void readMiFloras() {
  Serial.println("Going to read Mi Floras");
  BLEClient*  BLEClient = BLEDevice::createClient();
  // Setting the callbacks gives us the possibility to react on events
  BLEClient->setClientCallbacks(new MyClientCallback());
  Serial.println("  Created client");
  
  for (floraindex = 0; floraindex < knowndevices && myDevices[floraindex] != nullptr; floraindex++) { //loop through the slots for the Mi Floras
   Serial.print("  Handling Mi Flora with address: ");
   Serial.println(myDevices[floraindex]->getAddress().toString().c_str());
   BLEClient->connect(myDevices[floraindex]);

   BLERemoteService* FloraService = BLEClient->getService(dataUUID);
   if (FloraService != nullptr) {
    Serial.println("    Found our service");
    readSensordata(FloraService);
    readBattery(FloraService);
   } else {
    Serial.println("    Failed to find our service");
   }
   
   BLEClient->disconnect();
   delay(500);
  }
  Serial.println("Reading finished");
}
